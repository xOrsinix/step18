package com.example.myapplication

data class MyDataItem(
    var body: String,
    val id: Int,
    var title: String,
    val userId: Int
)