package com.example.myapplication

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class DataModel():ViewModel(){
    val currentPost:MutableLiveData<MyDataItem> by lazy {
        MutableLiveData<MyDataItem>()
    }

    val retrofitBuilder: MutableLiveData<ApiInterface> by lazy {
        MutableLiveData<ApiInterface>()
    }
}
