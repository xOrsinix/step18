package com.example.myapplication

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface ApiInterface {

    @GET("posts")
    fun getData(): Call<List<MyDataItem>>

    @POST("posts")
    fun updateUser(@Body myDataItem: MyDataItem):Call<MyDataItem>
}