package com.example.myapplication

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.databinding.FragmentPostsListBinding
import com.example.myapplication.databinding.PostItemBinding

class MyDataItemAdapter(dataModel:DataModel,postsListBinding: FragmentPostsListBinding):RecyclerView.Adapter<MyDataItemAdapter.MyDataItemHolder>() {

    val postsListBinding_my = postsListBinding

    val dataModel_my = dataModel

    val myDataItemList = arrayListOf<MyDataItem>()

    class MyDataItemHolder(item: View):RecyclerView.ViewHolder(item) {
        val binding = PostItemBinding.bind(item)
        fun bind(myDataItem: MyDataItem, postsListBinding: FragmentPostsListBinding,dataModel: DataModel){
            binding.tvUserId.text = myDataItem.userId.toString()
            binding.tvPostId.text = myDataItem.id.toString()
            binding.tvTitle.text = myDataItem.title
            binding.tvBody.text=myDataItem.body
            binding.linLay.setOnClickListener {
                dataModel.currentPost.value = myDataItem
                Navigation.findNavController(postsListBinding.root).navigate(R.id.fromPostsToEdit)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyDataItemHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.post_item,parent,false)
        return MyDataItemHolder(view)
    }

    override fun onBindViewHolder(holder: MyDataItemHolder, position: Int) {
        holder.bind(myDataItemList[position],postsListBinding_my,dataModel_my)
    }

    override fun getItemCount(): Int = myDataItemList.size

    fun addMyDataItem(item: MyDataItem) {
        myDataItemList.add(item)
        notifyDataSetChanged()
    }
}