package com.example.myapplication

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import com.example.myapplication.databinding.FragmentEditPostBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EditPostFragment : Fragment() {

    lateinit var binding: FragmentEditPostBinding

    private val dataModel:DataModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentEditPostBinding.inflate(inflater)
        val dataItem = dataModel.currentPost.value!!
        binding.etTitle.setText(dataItem.title)
        binding.etBody.setText(dataItem.body)
        binding.btChange.setOnClickListener {
            dataModel.currentPost.value!!.title=binding.etTitle.text.toString()
            dataModel.currentPost.value!!.body=binding.etBody.text.toString()
            dataModel.retrofitBuilder.value!!.updateUser(dataModel.currentPost.value!!).enqueue(object : Callback<MyDataItem?> {
                override fun onResponse(call: Call<MyDataItem?>, response: Response<MyDataItem?>) {
                    Log.i("EDITFRAG",response.body().toString())
                    Log.i("EDITFRAG",response.code().toString())
                    Log.i("EDITFRAG",response.message())
                }

                override fun onFailure(call: Call<MyDataItem?>, t: Throwable) {
                    Log.i("EDITFRAG",t.message!!)
                }
            })
            Navigation.findNavController(binding.root).navigate(R.id.fromEditToPosts)
        }
        binding.btExit.setOnClickListener {
            Navigation.findNavController(binding.root).navigate(R.id.fromEditToPosts)
        }
        return binding.root
    }

    companion object {
        @JvmStatic
        fun newInstance() = EditPostFragment()
    }
}